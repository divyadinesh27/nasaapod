//
//  AnimatedLaunchViewController.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 05/09/21.
//

import UIKit

class AnimatedLaunchViewController: UIViewController {
    @IBOutlet weak var animatedImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addZoomAnimation()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) { [weak self] in
            self?.performSegue(withIdentifier: "toAPODPage", sender: nil)
        }
    }
    
    ///Adding Zoom animation to launch screen.
    func addZoomAnimation() {
        let animation = CABasicAnimation.init(keyPath: "transform.scale")
        animation.fromValue = 0.2
        animation.toValue = 1
        animation.duration = 1.5
        animation.autoreverses = false
        animation.repeatCount = 0
        animatedImage.layer.add(animation, forKey: "ZoomAnimation")
    }
}
