//
//  Utility.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 04/09/21.
//

import Foundation

/// Common functions used across the application.
class Utility {
    
    /// Returns image directory path
    /// - Returns: url of the image directory path
    class func getImageDirectoryPath() -> URL? {
        let fileManager = FileManager.default
        let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
        if let documentPath = documentsUrl?.appendingPathComponent("Images") {
            do {
                try fileManager.createDirectory(at: documentPath, withIntermediateDirectories: true, attributes: nil)
                return documentPath
            }catch{
                DPrint("ERROR:\(error.localizedDescription)")
            }
        }
        return nil
    }
}
