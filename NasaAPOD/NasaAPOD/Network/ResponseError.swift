//
//  ResponseError.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 06/09/21.
//

import Foundation

enum ResponseError: Int {
    case errorInvalidUrl = 400
    case errorNoDataFromServer = 404
    case errorNoObjectFound = 405
    
    var localizedDescription: String {
        switch self {
        case .errorInvalidUrl:
            return NSLocalizedString("INVALID_DATE_ERROR", comment: "")
        case .errorNoDataFromServer:
            return NSLocalizedString("ERROR_NO_DATA", comment: "")
        case .errorNoObjectFound:
            return NSLocalizedString("ERROR_IN_LOADING", comment: "")
        }
    }
}
