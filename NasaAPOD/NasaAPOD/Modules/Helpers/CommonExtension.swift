//
//  CommonExtension.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 4/09/21.
//

import Foundation
import UIKit

fileprivate let activityIndicator = UIActivityIndicatorView.init(style: .large)

///Common extension for UiViewController
extension UIViewController {
    
    /// To get topview controller of application
    /// - Returns: UIViewController
    class func topViewController() -> UIViewController? {
        let keyWindow = UIApplication.shared.windows.first { $0.isKeyWindow }
        var topController: UIViewController? = keyWindow?.rootViewController
        while topController?.presentedViewController != nil {
            topController = topController?.presentedViewController
        }
        return topController
    }
    
    /// Display alert merssage
    /// - Parameters:
    ///   - title: Alert title
    ///   - message: Alert Message
    ///   - style: Alert style
    ///   - actions: Alert actions
    ///   - barButtonItem: bar button item to present alert in ipad.
    func showAlertMessage(title: String? = "", message: String? = "", style: UIAlertController.Style = .alert, actions: [UIAlertAction]? = [UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)] , barButtonItem: UIBarButtonItem? = nil) {
        DispatchQueue.main.async {[weak self] in
            let alertVC = UIAlertController.init(title: title, message: message, preferredStyle: style)
            if let alertAction = actions {
                for action in alertAction {
                    alertVC.addAction(action)
                }
            }
            alertVC.popoverPresentationController?.sourceView = self?.view
            if let barButton = barButtonItem {
                alertVC.popoverPresentationController?.barButtonItem = barButton
            }
            UIViewController.topViewController()?.present(alertVC, animated: true, completion: nil)
        }
    }
    
    /// Show loader for network or DB calls.
    func showLoader(_ loadingText: String? = "Loading") {
        DispatchQueue.main.async {[weak self] in
            guard let strongSelf = self else { return }
            activityIndicator.center = strongSelf.view.center
            activityIndicator.color = .darkGray
            strongSelf.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
        }
    }
    
    /// Hide Loader.
    func hideLoader() {
        activityIndicator.stopAnimating()
    }
}
