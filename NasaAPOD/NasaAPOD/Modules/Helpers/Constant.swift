//
//  Constant.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 4/09/21.
//

import Foundation
import UIKit

let kLabelWidth: CGFloat = 150.0
let kNasaThemeColorBlue: UIColor = UIColor(named: "buttonColor") ?? UIColor(red: 11/255, green: 61/255, blue: 145/255, alpha: 1)
let kNasaThemeColorOrange: UIColor = UIColor(red: 252/255, green: 61/255, blue: 33/255, alpha: 1)
let kServerDateFormat = "YYYY-MM-dd"
let kDisplayDateFormat = "dd-MM-YYYY"
let kDatePickerPadding: CGFloat = 44.0

func DPrint(_ items: Any...) {
    #if DEBUG
    for i in items {
        Swift.print(i)
    }
    #endif
}
