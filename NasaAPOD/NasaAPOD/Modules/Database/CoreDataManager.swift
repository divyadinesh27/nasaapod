//
//  DatabaseManager.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 4/09/21.
//

import Foundation
import CoreData

final class CoreDataManager {
    
    /// Coredata manager singleton instance.
    static let shared = CoreDataManager()
    private init() {
        
    }
    
    // MARK: - Contexts

    /// Managed object context.
    var viewContext: NSManagedObjectContext {
        let context = persistentContainer.viewContext
        context.mergePolicy = NSMergePolicy.mergeByPropertyStoreTrump
        return context
    }
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "NasaAPOD")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                DPrint("Core data error:\(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func save (context: NSManagedObjectContext) {
        context.performAndWait  {
            if context.hasChanges {
                do {
                    try context.save()
                    DPrint("Successfully Saved")
                } catch {
                    let error = error as NSError
                    DPrint("Error: \(error)")
                }
            }
        }
    }
    
    /// Generic query to fetch data from coredata
    /// - Parameters:
    ///   - _entity: Managed Object type
    ///   - search: search predicate
    ///   - sort: sort options
    ///   - fetchLimit: fetch limit
    ///   - context: context
    /// - Returns: Array of objects.
    func query<T>(_entity: T.Type, search: NSPredicate? = nil, sort: [NSSortDescriptor]? = nil, fetchLimit: Int = 0, context: NSManagedObjectContext? = nil) -> [T] where T: NSManagedObject {
        let fetchRequest = NSFetchRequest<T>(entityName: NSStringFromClass(T.self))
        
        if let predicate = search {
            fetchRequest.predicate = predicate
        }
        
        if let sortDesc = sort {
            fetchRequest.sortDescriptors = sortDesc
        }
        
        if fetchLimit > 0 {
            fetchRequest.fetchLimit = fetchLimit
        }
        
        var results: [Any] = []
        var objectContext: NSManagedObjectContext
        if let context = context {
            objectContext = context
        } else {
            objectContext = self.viewContext
        }
        do {
            results = try objectContext.fetch(fetchRequest)
        } catch {
            DPrint("Error in executing fetch request: \(error.localizedDescription)")
        }
        return results as? [T] ?? []
    }
}
