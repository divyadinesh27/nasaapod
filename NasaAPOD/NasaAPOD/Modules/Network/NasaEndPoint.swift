//
//  NasaEndpoint.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 4/09/21.
//

import Foundation

/// Defining end points and its properties.
enum NasaEndpoint: Endpoint {
    /// Endpoint to fetch Astronomy Picture of the Day Data.
    case getAPODData(_ date: String? = nil)
    
    /// Endpoint to fetch image corresponding to APOD.
    case getImageData(path: String)
    
    var baseURL: URL? {
        return URL(string: "https://api.nasa.gov")
    }
    
    var fullUrl: URL? {
        switch self {
        case .getImageData(let path):
            return URL(string: path)
        default:
            return nil
        }
    }
    
    var path: String {
        return "planetary/apod"
    }
    
    var parameters: [URLQueryItem]? {
        switch self {
        case .getAPODData(let date):
            let apiKey = "QdZSDDDXoieGReg97Dh3kArhCzThThdEZjd7GW40"
            return [URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "date", value: date)]
        default:
            return nil
        }
    }
    
    var method: String {
        return "GET"
    }
}
