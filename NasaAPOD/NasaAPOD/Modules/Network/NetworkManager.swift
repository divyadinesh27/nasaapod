//
//  NetworkManager.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 4/09/21.
//

import Foundation
import CoreData

/// Network Manager class makes network calls to fetch data from server.
class NetworkManager {
    /// Executes the web service call and will decode the JSON response into the Codable object provided
    /// - Parameters:
    /// - endpoint: the endpoint to make the HTTP request against
    /// - completion: the JSON response converted to the provided Codable object, if successful, or failure otherwise
    
    class func request(endpoint: Endpoint, completion: @escaping(Result <Data, Error>) -> ()) {
        DispatchQueue.global(qos: .background).async {
            var requestURL: URL?
            if let fullUrl = endpoint.fullUrl { // If we are having complete url we are ignoring base url and path.
                requestURL = fullUrl
            } else if let baseUrl  = endpoint.baseURL {
                requestURL = baseUrl.appendingPathComponent(endpoint.path)
            }
            guard let requestUrl = requestURL else {
                DPrint("Error in constructing url")
                return
            }
            
            var requestUrlComponents = URLComponents.init(url: requestUrl, resolvingAgainstBaseURL: false)
            requestUrlComponents?.queryItems = endpoint.parameters
            
            guard let url = requestUrlComponents?.url else {
                DPrint("Error in constructing url")
                return
            }
            
            var urlRequest = URLRequest.init(url: url)
            urlRequest.httpMethod = endpoint.method
            
            let sessionConfig = URLSessionConfiguration.default
            sessionConfig.timeoutIntervalForRequest = 30.0
            sessionConfig.timeoutIntervalForRequest = 30.0

            let session = URLSession(configuration: sessionConfig)
            let dataTask = session.dataTask(with: urlRequest) {data, response, error in
                if let responseError = error {
                    completion(.failure(responseError))
                    DPrint(responseError .localizedDescription)
                    return
                } else {
                    guard response != nil, let data = data else {
                        return
                    }
                    completion(.success(data))
                }
            }
            dataTask.resume()
        }
    }
}
