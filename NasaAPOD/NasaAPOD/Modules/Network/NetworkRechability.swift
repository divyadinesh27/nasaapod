//
//  NetworkRechability.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 04/09/21.
//

import Foundation
import Network

///Class helps to check network connectivity.
class NetworkReachability {
    static var shared = NetworkReachability()
    var pathMonitor: NWPathMonitor!
    var path: NWPath?
    lazy var pathUpdateHandler: ((NWPath) -> Void) = {[weak self] path in
        self?.path = path
        if path.status == NWPath.Status.satisfied {
            DPrint("Connected")
        } else if path.status == NWPath.Status.unsatisfied {
            DPrint("unsatisfied")
        } else if path.status == NWPath.Status.requiresConnection {
            DPrint("requiresConnection")
        }
    }
    
    let backgroudQueue = DispatchQueue.global(qos: .background)
    
    private init() {
        pathMonitor = NWPathMonitor()
        pathMonitor.pathUpdateHandler = self.pathUpdateHandler
        pathMonitor.start(queue: backgroudQueue)
    }
    
    /// Method returns network connectivity status.
    func isNetworkAvailable() -> Bool {
        if let path = self.path {
            if path.status == NWPath.Status.satisfied {
                return true
            }
        }
        return false
    }
}
