//
//  MediaType.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 05/09/21.
//

import Foundation

enum MediaType: String {
    case image = "image"
    case video = "video"
}
