//
//  APODModel+CoreDataClass.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 05/09/21.
//
//

import Foundation
import CoreData

@objc(APODModel)

/// NSManaged object which uses codable protocol to encode and decode object.
public class APODModel: NSManagedObject, Codable {
    enum CodingKeys: String, CodingKey {
        case copyRight = "copyright"
        case date
        case explanation
        case url
        case hdurl
        case mediaType = "media_type"
        case title
        case isFavourite
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        do {
            try container.encode(date, forKey: .date)
            try container.encode(copyRight, forKey: .copyRight)
            try container.encode(explanation, forKey: .explanation)
            try container.encode(url, forKey: .url)
            try container.encode(hdurl, forKey: .hdurl)
            try container.encode(mediaType, forKey: .mediaType)
            try container.encode(title, forKey: .title)
        } catch {
            DPrint(error)
        }
    }
    
    required convenience public init(from decoder: Decoder) throws {
        guard let contextCodingUserInfoKey = CodingUserInfoKey.managedObjectContext,
              let managedObjectContext = decoder.userInfo[contextCodingUserInfoKey] as? NSManagedObjectContext,
              let entity = NSEntityDescription.entity(forEntityName: "APODModel", in: managedObjectContext)
        else {
            fatalError("Error in decoding object")
        }
        self.init(entity: entity, insertInto: managedObjectContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        date = try container.decodeIfPresent(String.self, forKey: .date) ?? ""
        copyRight = try container.decodeIfPresent(String.self, forKey: .copyRight) ?? ""
        explanation = try container.decodeIfPresent(String.self, forKey: .explanation) ?? ""
        url = try container.decodeIfPresent(String.self, forKey: .url) ?? ""
        hdurl = try container.decodeIfPresent(String.self, forKey: .hdurl) ?? ""
        title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
        mediaType = try container.decodeIfPresent(String.self, forKey: .mediaType) ?? ""
    }
}
