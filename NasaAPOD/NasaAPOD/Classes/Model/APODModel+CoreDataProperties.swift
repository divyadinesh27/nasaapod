//
//  APODModel+CoreDataProperties.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 05/09/21.
//
//

import Foundation
import CoreData


extension APODModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<APODModel> {
        return NSFetchRequest<APODModel>(entityName: "APODModel")
    }

    @NSManaged public var copyRight: String
    @NSManaged public var date: String
    @NSManaged public var explanation: String
    @NSManaged public var url: String
    @NSManaged public var hdurl: String
    @NSManaged public var mediaType: String
    @NSManaged public var title: String
    @NSManaged public var isFavourite: Bool

}

extension APODModel : Identifiable {

}
