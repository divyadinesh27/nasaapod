//
//  NasaAPODDataFetcher.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 04/09/21.
//

import Foundation
import CoreData

class NasaAPODDataFetcher {
    let date: String
    
    init(date: String) {
        self.date = date
    }
    
    /// Get APOD Data from server or DB based on availability.
    func getAPODData(completion: @escaping(_ apodData: APODModel?,_ error: Error?) -> ()) {
        if NetworkReachability.shared.isNetworkAvailable() {
            NetworkManager.request(endpoint: NasaEndpoint.getAPODData(date)) { [weak self] (result: Result<Data, Error>) in
                switch(result) {
                case .success(let data):
                    self?.processResponse(data, completion: completion)
                case .failure(let error):
                    completion(nil, error)
                    DPrint(error)
                }
            }
        } else {
            if let apodData = fetchAPODDataFromDB(date) {
                completion(apodData, nil)
            } else {
                let error = NSError.init(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("ERROR_IN_LOADING", comment: "")])
                completion(nil, error)
            }
        }
    }
    
    /// Process the response and decode the object.
    /// - Parameter data: data from the server
    private func processResponse(_ data: Data, completion: @escaping(_ apodData: APODModel?,_ error: Error?) -> ()) {
        if let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] {
            DPrint(json)
            if let errorCode = json["code"] as? Int {
                switch errorCode {
                case 400:
                    let error = NSError(domain: "", code: errorCode, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("INVALID_DATE_ERROR", comment: "")])
                    completion(nil, error)
                    return
                case 404:
                    if let msg = json["msg"] as? String {
                    let error = NSError(domain: "", code: errorCode, userInfo: [NSLocalizedDescriptionKey: msg])
                    completion(nil, error)
                    }
                    return
                default:
                    break
                }
            }
        }
       
        var apodData: APODModel?
        let decoder = JSONDecoder()
        if let context = CodingUserInfoKey.managedObjectContext {
            decoder.userInfo[context] = CoreDataManager.shared.viewContext
        }
        do {
            apodData = try decoder.decode(APODModel.self, from: data)
            completion (apodData, nil)
        } catch {
            completion(nil, error)
            DPrint(error)
        }
        CoreDataManager.shared.save(context: CoreDataManager.shared.viewContext)
    }
    
    
    /// - Returns:
    /// Fetches data from DB to show offline Data.
    /// - Parameter date: date for which APOD data needs to be fetched.
    /// - Returns: APOD Data for the requested Date
    private func fetchAPODDataFromDB(_ date: String) -> APODModel? {
        var predicate: NSPredicate
        predicate = NSPredicate.init(format: "date == %@", date)
        let result = CoreDataManager.shared.query(_entity: APODModel.self, search: predicate, context: CoreDataManager.shared.viewContext)
        return result.first
    }
}
