//
//  ImageLoader.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 04/09/21.
//

import Foundation
import UIKit

class ImageLoader {
    var imagePath: String?
    
    init(path: String) {
        imagePath = path
    }
    
    /// Loads image from server or DB based on availability.
    func loadImage(completion: @escaping((UIImage?, Error?) -> ())) {
        if let path = imagePath  {
            if NetworkReachability.shared.isNetworkAvailable() {
                NetworkManager.request(endpoint: NasaEndpoint.getImageData(path: path)) { (result: Result<Data, Error>) in
                    switch result {
                    case .success(let data):
                        if let image = self.getImageFromData(data: data) {
                            completion(image, nil)
                        }
                    case .failure(let error):
                        completion(nil, error)
                    }
                }
            } else {
                if let image = getImageFromFiles() {
                    completion(image, nil)
                } else {
                    DPrint("Error in retrieving image file")                }
            }
        }
    }
    
    
    /// Loads image from files to load in offline mode.
    /// - Returns: APOD image
    private func getImageFromFiles() -> UIImage? {
        if let path = imagePath, let fileName = path.components(separatedBy: "/").last {
            if let fileURL = Utility.getImageDirectoryPath()?.appendingPathComponent(fileName) {
                if FileManager.default.fileExists(atPath: fileURL.path) {
                    let image = UIImage.init(contentsOfFile: fileURL.path)
                    return image
                }
            }
        }
        return nil
    }
    
    
    /// Method converts data to image.
    /// - Parameter data: image data
    /// - Returns: Image
    private func getImageFromData(data: Data) -> UIImage? {
        let image = UIImage.init(data: data)
        saveImageInFiles(data: data)
        return image
    }
    
    
    /// Saves poster image in file system.
    /// - Parameter data: image data
    private func saveImageInFiles(data: Data) {
        if let path = imagePath,  let fileName = path.components(separatedBy: "/").last {
            if let fileURL = Utility.getImageDirectoryPath()?.appendingPathComponent(fileName) {
                if !FileManager.default.fileExists(atPath: fileURL.path) {
                    do {
                        try data.write(to: fileURL)
                        DPrint("\(path) Image saved")
                    } catch {
                        DPrint("error saving image:", error)
                    }
                }
            }
        }
    }
}
