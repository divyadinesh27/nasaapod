//
//  APODCollectionViewCell.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 05/09/21.
//

import UIKit

class APODCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var apodImageView: UIImageView!
    @IBOutlet weak var apodTitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
}
