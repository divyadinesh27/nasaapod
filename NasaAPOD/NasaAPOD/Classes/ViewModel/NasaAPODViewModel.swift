//
//  NasaAPODViewModel.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 04/09/21.
//

import Foundation
import UIKit

class NasaAPODViewModel {
    
    /// APOD data fetcher.
    private var apodDataFetcher: NasaAPODDataFetcher?
    
    /// APOD Image loader
    private var imageLoader: ImageLoader?
    
    /// Observer listens to loading of APOD data.
    var isDataLoading = Observable(false)
    
    /// Observer listens to loading of APOD image of the day.
    var isImageLoading = Observable(false)
    
    /// Property stores APOD Data.
    var apodData: APODModel?
    
    /// Retaining User selected date.
    var selectedDate: Date?
    
    /// Property stores error states.
    var error: Error?
    
    /// APOD image
    var apodImage: UIImage?
    
    /// Fetches APOD Date from Data store(API / Database depending on connection status).
    /// - Parameter date: Date for which APOD data needs to be fetched.
    func fetchAPODData() {
        isDataLoading.value = true
        let date = selectedDate ?? Date()
        let dateString = getStringFromDate(date, inServerFormat: true)
        apodDataFetcher = NasaAPODDataFetcher(date: dateString)
        apodDataFetcher?.getAPODData(completion: { [weak self] apodModel, error in
            if error != nil {
                self?.error = error
                self?.apodData = nil
            } else {
                self?.error = nil
                self?.apodData = apodModel
                if apodModel?.mediaType == MediaType.image.rawValue {
                    self?.fetchAPODImage()
                } 
            }
            self?.isDataLoading.value = false
        })
    }
    
    /// Fetches APOD image from Data store(API/Files depends on connectivity)
    func fetchAPODImage() {
        self.isImageLoading.value = true
        self.imageLoader = ImageLoader.init(path: apodData?.url ?? "")
        self.imageLoader?.loadImage(completion: {[weak self] (image, error) in
            if error != nil {
                self?.error = error
            } else {
            self?.apodImage = image
            }
            self?.isImageLoading.value = false
        })
    }    
    
    /// Convers and returns string fromdate
    /// - Parameter date: date needs to be converted
    /// - inServerFormat: the format in which date need to converted.
    /// - Returns: Output string
    
    func getStringFromDate(_ date: Date, inServerFormat: Bool? = false) -> String {
        let dateFormatter = DateFormatter()
        if inServerFormat ?? false {
            dateFormatter.dateFormat = kServerDateFormat
        } else {
            dateFormatter.dateFormat = kDisplayDateFormat
        }
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    /// Convers and returns date from string
    /// - Parameter date: dateString needs to be converted
    /// - Returns: Output date
    func getDateFromString(_ dateString: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = kServerDateFormat
        let date = dateFormatter.date(from: dateString)
        return date
    }
    
    /// Method to mark and unmark APOD data as favourites.
    /// - Parameter isFavourite: favourite flag
    func markAsFavourite(_ isFavourite: Bool) {
        apodData?.isFavourite = isFavourite
        CoreDataManager.shared.save(context: CoreDataManager.shared.viewContext)
    }
    
    /// Returns video url to play video inline.
    func getVideoURL() -> String? {
        let videoUrl = apodData?.url
        if let component = videoUrl?.components(separatedBy: "?").first {
            let url = component + "?playsinline=1"
            return url
        }
        return nil
    }
}
