//
//  FavouritesViewModel.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 05/09/21.
//

import Foundation
import CoreData
import UIKit

class APODListViewModel {
    var displayFavourites: Bool = false
    var listItems: [APODModel]?
    var isLoading = Observable(false)
    
    /// Initializer
    /// - Parameter favourites: Bool to indicate whether to load only favourites or all the objects
    init(favourites: Bool) {
        displayFavourites = favourites
    }
    
    /// Load list of APOD data models.
    func loadListItems() {
            isLoading.value = true
            var predicate: NSPredicate?
            if displayFavourites {
                 predicate = NSPredicate.init(format: "isFavourite == true")
            }
            let results = CoreDataManager.shared.query(_entity: APODModel.self, search: predicate, context: CoreDataManager.shared.viewContext)
        listItems = results
        isLoading.value = false
        DPrint(results)
    }
    
    /// Converts server date format into display format
    /// - Parameter serverDate: Date string in server format
    /// - Returns: Date string in display format.
    func getDateInDisplayFormat(_ serverDate: String) -> String {
        let serverFormat = DateFormatter()
        serverFormat.dateFormat = kServerDateFormat
        if let date = serverFormat.date(from: serverDate) {
            let userFormat = DateFormatter()
            userFormat.dateFormat = kDisplayDateFormat
            let dateString = userFormat.string(from: date)
            return dateString
        } else {
            return serverDate
        }
    }
    
    /// Loads image from file system.
    /// - Returns: APOD image
    func getImage(_ path: String) -> UIImage? {
        if let fileName = path.components(separatedBy: "/").last {
            if let fileURL = Utility.getImageDirectoryPath()?.appendingPathComponent(fileName) {
                if FileManager.default.fileExists(atPath: fileURL.path) {
                    let image = UIImage.init(contentsOfFile: fileURL.path)
                    return image
                }
            }
        }
        return nil
    }
}
