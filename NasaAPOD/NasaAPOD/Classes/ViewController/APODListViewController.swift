//
//  FavouritesViewController.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 05/09/21.
//

import Foundation
import UIKit

class APODListViewController: UIViewController {
    
    @IBOutlet weak var noItemsLabel: UILabel!
    @IBOutlet weak var apodCollectionView: UICollectionView!
    var listItemSelected: ((APODModel) -> ())?
    
    var viewModel: APODListViewModel?
    
    override func viewDidLoad() {
        apodCollectionView.dataSource = self
        apodCollectionView.delegate = self
        if viewModel?.displayFavourites ?? false {
            self.title = NSLocalizedString("FAVOURITES", comment: "")
        } else {
            self.title = NSLocalizedString("HISTORY", comment: "")
        }
        loadList()
    }
    
    /// Load list of APOD models.
    private func loadList() {
        viewModel?.isLoading.bind(observer: {[weak self] (_, isLoading) in
            if !isLoading {
                DispatchQueue.main.async {[weak self] in
                    guard let weakSelf = self else { return }
                    weakSelf.hideLoader()
                    if weakSelf.viewModel?.listItems?.count ?? 0 > 0 {
                        weakSelf.noItemsLabel.isHidden = true
                        weakSelf.apodCollectionView.isHidden = false
                        weakSelf.apodCollectionView.reloadData()
                    } else {
                        weakSelf.noItemsLabel.isHidden = false
                        weakSelf.apodCollectionView.isHidden = true
                        weakSelf.noItemsLabel.text = weakSelf.viewModel?.displayFavourites ?? false ? NSLocalizedString("NO_FAVOURITES", comment: "") : NSLocalizedString("NO_HISTORY", comment: "")
                    }
                }
            } else {
                self?.showLoader()
            }
        })
        viewModel?.loadListItems()
    }
    
    /// IBaction to dismiss the view controller
    /// - Parameter sender: Button
    @IBAction func closeBUttonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: Table view delegates
extension APODListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.listItems?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "APODCell", for: indexPath) as? APODCollectionViewCell {
            let apodItem = viewModel?.listItems?[indexPath.row]
            cell.apodTitleLabel.text = apodItem?.title
            cell.dateLabel.text = viewModel?.getDateInDisplayFormat(apodItem?.date ?? "")
            cell.apodImageView?.image = viewModel?.getImage(apodItem?.url ?? "") ?? #imageLiteral(resourceName: "video-thumbnail")
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: 200 , height: 250)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let selectedItem = viewModel?.listItems?[indexPath.item] {
            self.listItemSelected?(selectedItem)
            dismiss(animated: true, completion: nil)
        }
    }
}
