//
//  ViewController.swift
//  NasaAPOD
//
//  Created by Divya Dinesh on 04/09/21.
//

import UIKit
import WebKit
import AVFoundation
class NasaAPODViewController: UIViewController {
    
    /// NasaAPODViewModel handles business logic of NasaAPODViewController.
    let viewModel = NasaAPODViewModel()
    @IBOutlet weak var historyButton: UIBarButtonItem!
    
    @IBOutlet weak var openFavouritesPage: UIBarButtonItem!
    @IBOutlet weak var playerView: WKWebView!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var explanationTextView: UITextView!
    @IBOutlet weak var apodImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var favouritesButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var dateTextField: UITextField!
    let datePicker = UIDatePicker()
    var webPlayer = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        observeDataLoading()
        fetchAPODData()
        setupUI()
        setUpWebView()
    }
    
    /// Update UI of Datelabel.
    private func setupUI() {
        containerView.isHidden = true
        favouritesButton.tintColor = kNasaThemeColorBlue
        // Set initial state of favourites button
        let image = viewModel.apodData?.isFavourite ?? false ? UIImage(systemName: "star.fill") : UIImage(systemName: "star")
        favouritesButton.setBackgroundImage(image, for: .normal)
        dateTextField.delegate = self
        addDatePicker()
    }
    
    private func setUpWebView() {
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.mediaTypesRequiringUserActionForPlayback = .audio
        webConfiguration.allowsInlineMediaPlayback = true
        webConfiguration.mediaTypesRequiringUserActionForPlayback = []
        
        webPlayer = WKWebView(frame: self.playerView.bounds, configuration: webConfiguration)

        self.playerView.addSubview(webPlayer)
    }
    
    override func viewDidLayoutSubviews() {
        self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.explanationTextView.frame.origin.y + self.explanationTextView.contentSize.height)
    }
    
    /// Shows date picker.
    /// - Parameter sender: UIButton
    private func addDatePicker() {
        let imageView = UIImageView.init(image: UIImage(systemName: "calendar"))
        imageView.tintColor = kNasaThemeColorBlue
        dateTextField.rightView = imageView
        dateTextField.rightViewMode = .always
        dateTextField.text = viewModel.getStringFromDate(viewModel.selectedDate ?? Date())
        
        datePicker.locale = .current
        datePicker.date = viewModel.selectedDate ?? Date()
        datePicker.datePickerMode = .date
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.maximumDate = Date()
        dateTextField.inputView = datePicker
        
        //Add Tool Bar as input AccessoryView
        let toolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelBarButton = UIBarButtonItem(title: NSLocalizedString("CANCEL", comment: ""), style: .plain, target: self, action: #selector(cancelPressed))
        cancelBarButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : kNasaThemeColorBlue], for: .normal)
        let doneBarButton = UIBarButtonItem(title: NSLocalizedString("DONE", comment: ""), style: .plain, target: target, action: #selector(donePressed))
        doneBarButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : kNasaThemeColorBlue], for: .normal)
        
        toolBar.setItems([cancelBarButton, flexibleSpace, doneBarButton], animated: false)
        dateTextField.inputAccessoryView = toolBar
    }
    
    ///Dismiss date picker.
    @objc func cancelPressed() {
        self.dateTextField.resignFirstResponder()
    }
    
    ///Call back on selection of date from date picker.
    @objc func donePressed() {
        self.dateTextField.resignFirstResponder()
        let date = datePicker.date
        viewModel.selectedDate = date
        self.dateTextField.text = viewModel.getStringFromDate(date)
        fetchAPODData(date)
    }
    
    ///IBAction for favourite button.
    @IBAction func favouritesButtonTapped(_ sender: Any) {
        if let apodData = viewModel.apodData {
            var isFavourite = false
            if apodData.isFavourite {
                favouritesButton.setBackgroundImage(UIImage(systemName: "star"), for: .normal)
                isFavourite = false
            } else {
                favouritesButton.setBackgroundImage(UIImage(systemName: "star.fill"), for: .normal)
                isFavourite = true
            }
            viewModel.markAsFavourite(isFavourite)
        }
    }
    
    /// Method observes data loading.
    private func observeDataLoading() {
        viewModel.isDataLoading.bind { [weak self](_, isLoading) in
            if !isLoading {
                let error = self?.viewModel.error
                if error != nil {
                    DispatchQueue.main.async {
                        self?.hideLoader()
                        self?.errorLabel.isHidden = false
                        self?.dateTextField.isEnabled = true
                        self?.historyButton.isEnabled = true
                        self?.openFavouritesPage.isEnabled = true
                        
                        self?.errorLabel.text = error?.localizedDescription ?? NSLocalizedString("ERROR_IN_LOADING", comment: "")
                        DPrint(error?.localizedDescription ?? "Error in fetching data")
                    }
                } else {
                    self?.updateUI()
                    if self?.viewModel.apodData?.mediaType == MediaType.video.rawValue {
                        self?.updateImageOrVideo()
                    }
                }
            } else {
                self?.showLoader()
                self?.disableUIWhileLoading()
            }
        }
        
        viewModel.isImageLoading.bind { [weak self](_, isImageLoading) in
            if !isImageLoading {
                let error = self?.viewModel.error
                if error != nil {
                    DispatchQueue.main.async {
                        self?.hideLoader()
                        self?.apodImageView.isHidden = false
                        if let apodImage = self?.viewModel.apodImage {
                            self?.apodImageView.image = apodImage
                        }
                        DPrint(error?.localizedDescription ?? "Error in fetching image")
                    }
                } else {
                    self?.updateImageOrVideo()
                }
            }
        }
    }
    
    /// Fetches APOD date to display in UI.
    /// - Parameter date: date (optional)
    private func fetchAPODData(_ date: Date? = nil) {
        viewModel.selectedDate = date
        viewModel.fetchAPODData()
    }
    
    
    /// Updates UI elments.
    private func updateUI() {
        DispatchQueue.main.async {[weak self] in
            self?.containerView.isHidden = false
            if let apodData = self?.viewModel.apodData  {
                self?.titleLabel.text = apodData.title
                self?.explanationTextView.text = apodData.explanation
                if apodData.isFavourite {
                    self?.favouritesButton.setBackgroundImage(UIImage(systemName: "star.fill"), for: .normal)
                } else {
                    self?.favouritesButton.setBackgroundImage(UIImage(systemName: "star"), for: .normal)
                }
            }
        }
    }
    
    /// Restricting UI interaction while loading data.
    private func disableUIWhileLoading() {
        DispatchQueue.main.async {[weak self] in
            self?.errorLabel.isHidden = true
            self?.containerView.isHidden = true
            self?.apodImageView.isHidden = true
            self?.playerView.isHidden = true
            self?.dateTextField.isEnabled = false
            self?.historyButton.isEnabled = false
            self?.openFavouritesPage.isEnabled = false
        }
    }
    
    /// Updates APOD Image.
    private func updateImageOrVideo() {
        DispatchQueue.main.async {[weak self] in
            guard let weakSelf = self else { return }
            weakSelf.dateTextField.isEnabled = true
            weakSelf.historyButton.isEnabled = true
            weakSelf.openFavouritesPage.isEnabled = true
            if weakSelf.viewModel.apodData?.mediaType == MediaType.video.rawValue {
                if NetworkReachability.shared.isNetworkAvailable() {
                    weakSelf.playerView.isHidden = false
                    // To play the video inline(in container view)
                    if let urlString = weakSelf.viewModel.getVideoURL() {
                        if let videoURL:URL = URL(string: urlString) {
                            let request:URLRequest = URLRequest(url: videoURL)
                            self?.webPlayer.load(request)
                        }
                    }
                } else {
                    weakSelf.apodImageView.isHidden = false
                    weakSelf.apodImageView.image = #imageLiteral(resourceName: "error")
                }
            } else {
                self?.apodImageView.isHidden = false
                if let apodImage = self?.viewModel.apodImage {
                    self?.apodImageView.image = apodImage
                }
            }
            self?.hideLoader()
        }
    }
    
    deinit {
        viewModel.isDataLoading.removeObservers()
        viewModel.isImageLoading.removeObservers()  
    }
}

extension NasaAPODViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let apodListVCNav = segue.destination as? UINavigationController, let apodListVC = apodListVCNav.topViewController as? APODListViewController{
            var displayFavorites = false
            if segue.identifier == "toFavourites" {
                displayFavorites = true
            }
            let viewModel = APODListViewModel(favourites: displayFavorites)
            apodListVC.viewModel = viewModel
            apodListVC.listItemSelected = {[weak self](selectedItem) in
                let date = self?.viewModel.getDateFromString(selectedItem.date) ?? Date()
                self?.fetchAPODData(date)
            }
        }
    }
}

extension NasaAPODViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
}

