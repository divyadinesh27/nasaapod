# README #

This README would normally document contains implementation details of NasaAPOD app.

### Design Pattern ###

Implemented NasaAPOD app following MVVM Design pattern combined with observer patten.
Segregated DB layer and network layer for easy code maintainence.
Every controller will have fetcher(fetches data from server/DB based on network availability), ViewModel(Process the data and has business logic), ViewController(To display in UI and user actions).

### Core features ###

* On launch of the application we are fetching APOD data for current date and display in UI.
* Displayed APOD title, APOD image/video, favourite indicator, APOD date and Explanation.
* User can search APOD data for any date.Maximum selectable date is current date according to user location.(All the searched results are persisted in core data).
* User can search APOD for previously searched date in offline.
* User can mark the APOD page as favourite by tapping favourites button next to APOD title.
* User can see list of favourite APOD by tapping favourite toolbar button at the bottom right(star) of the page and user can select it to view more details.
* User can see list of searched APOD's(History) by tapping History toolbar button at the bottom left(clock) of the page and user can select it to view more details.

### Other Feature ###

* Dark mode supported. 
* Designed UI to support multiple device sizes and orientations (iPhone, iPad, portrait, landscape).
* Designed and added app icon.
* Added launch screen with animation.
* Added localization support.
* Used Debug print instead of print to restrict logs only in debug mode.
* Error handling and UI updates in case of error.

### Coding standards ###

* Added code Documentation for all the classes, methods and variables..
* Modularized and structured code.
* Ensured Zero Warning
* Code segregation
